package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.util.UserUtil;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Transactional
    public void clear(@NotNull final String userId) {
        projectRepository.deleteByUserId(userId);
    }

    @Transactional
    public void add(@NotNull final String userId, @Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        projectRepository.save(model);
    }

    @Nullable
    public List<Project> findAll(@NotNull final String userId) {
        return projectRepository.findByUserId(userId);
    }

    @Transactional
    public void remove(@NotNull final String userId, @Nullable Project model) {
        if (model == null) throw new EntityNotFoundException();
        if (model.getUserId() == null || !model.getUserId().equals(userId)) throw new EntityNotFoundException();
        projectRepository.delete(model);
    }

    @Transactional
    public void removeById(@NotNull final String userId, @Nullable String id) {
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    public @Nullable Project findOneById(@NotNull final String userId, @Nullable String id) {
        return projectRepository.findFirstByUserIdAndId(userId, id);
    }

}
