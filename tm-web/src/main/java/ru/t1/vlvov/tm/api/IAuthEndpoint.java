package ru.t1.vlvov.tm.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.vlvov.tm.dto.Result;
import ru.t1.vlvov.tm.model.User;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpoint {

    @WebMethod
    @PostMapping("/login")
    public Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    );

    @WebMethod
    @PostMapping("/profile")
    public User profile();

    @WebMethod
    @PostMapping("/logout")
    public Result logout();

}
