package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserService {

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public void clear() {
        userRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable User model) {
        if (model == null) throw new EntityNotFoundException();
        userRepository.save(model);
    }

    @Transactional
    public void update(@Nullable User model) {
        if (model == null) throw new EntityNotFoundException();
        userRepository.save(model);
    }

    @Nullable
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Transactional
    public void remove(@Nullable User model) {
        if (model == null) throw new EntityNotFoundException();
        userRepository.delete(model);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        userRepository.deleteById(id);
    }

    public @Nullable User findOneById(@Nullable String id) {
        return userRepository.findFirstById(id);
    }

}
