package ru.t1.vlvov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.vlvov.tm.api.ITaskEndpoint;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.service.TaskService;
import ru.t1.vlvov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.vlvov.tm.api.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    @PostMapping("/save")
    public Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.add(UserUtil.getUserId(), task);
        return task;
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.remove(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findOneById(UserUtil.getUserId(), id);
    }


}
