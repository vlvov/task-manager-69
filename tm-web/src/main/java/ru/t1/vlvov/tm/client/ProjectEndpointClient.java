package ru.t1.vlvov.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.vlvov.tm.model.Project;

import java.util.Collection;

public interface ProjectEndpointClient {

    static ProjectEndpointClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectEndpointClient.class, baseUrl);
    }

    @PostMapping("/save")
    Project save(@RequestBody Project project);

    @PostMapping("/delete")
    void delete(@RequestBody Project project);

    @PostMapping("/deleteById/{id}")
    void delete(
            @PathVariable("id") String id
    );

    @GetMapping("/findAll")
    Collection<Project> findAll();

    @GetMapping("/findById/{id}")
    Project findById(
            @PathVariable("id") String id
    );

}
