package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.repository.TaskRepository;
import ru.t1.vlvov.tm.util.UserUtil;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Transactional
    public void clear(@NotNull final String userId) {
        taskRepository.deleteByUserId(userId);
    }

    @Transactional
    public void add(@NotNull final String userId, @Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        taskRepository.save(model);
    }

    @Nullable
    public Collection<Task> findAll(@NotNull final String userId) {
        return taskRepository.findByUserId(userId);
    }

    @Transactional
    public void remove(@NotNull final String userId, @Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        if (model.getUserId() == null || !model.getUserId().equals(userId)) throw new EntityNotFoundException();
        taskRepository.delete(model);
    }

    @Transactional
    public void removeById(@NotNull final String userId, @Nullable String id) {
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    public @Nullable Task findOneById(@NotNull final String userId, @Nullable String id) {
        return taskRepository.findFirstByUserIdAndId(userId, id);
    }

}
