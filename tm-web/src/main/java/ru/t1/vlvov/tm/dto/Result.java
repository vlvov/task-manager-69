package ru.t1.vlvov.tm.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Result {

    private boolean success = true;

    private Exception exception;

    public Result(final boolean result) {
        success = result;
    }

    public Result(final Exception exception) {
        success = false;
        this.exception = exception;
    }

}
