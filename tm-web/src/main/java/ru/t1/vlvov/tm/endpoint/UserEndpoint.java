package ru.t1.vlvov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.vlvov.tm.api.IUserEndpoint;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/users")
@WebService(endpointInterface = "ru.t1.vlvov.tm.api.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    @Autowired
    private UserService userService;

    @Override
    @WebMethod
    @PostMapping("/save")
    public User save(
            @WebParam(name = "user", partName = "user")
            @RequestBody User user
    ) {
        userService.add(user);
        return user;
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "user", partName = "user")
            @RequestBody User user
    ) {
        userService.remove(user);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        userService.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<User> findAll() {
        return userService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public User findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return userService.findOneById(id);
    }

}
