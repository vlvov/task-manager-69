package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.vlvov.tm.model.User;

public interface UserRepository  extends JpaRepository<User, String> {

    User findOneByLogin(@NotNull final String login);

    User findFirstById(@NotNull final String id);

}
