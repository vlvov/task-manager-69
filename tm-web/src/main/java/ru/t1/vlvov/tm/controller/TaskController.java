package ru.t1.vlvov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.repository.TaskRepository;
import ru.t1.vlvov.tm.service.TaskService;
import ru.t1.vlvov.tm.util.UserUtil;

import java.util.UUID;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/task/create")
    public String create() {
        taskService.add(UserUtil.getUserId(), new Task(UUID.randomUUID().toString()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskService.removeById(UserUtil.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.add(UserUtil.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        final Task task = taskService.findOneById(UserUtil.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectRepository.findAll());
        return modelAndView;
    }

}
