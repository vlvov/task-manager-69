package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.dto.CustomUser;
import ru.t1.vlvov.tm.enumerated.RoleType;
import ru.t1.vlvov.tm.model.Role;
import ru.t1.vlvov.tm.model.User;
import ru.t1.vlvov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userRepository.findOneByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);

        final List<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (final Role role: userRoles) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build(), user.getId());
    }

    @PostConstruct
    public void init() {
        initUser("admin", "admin", RoleType.ADMINISTRATOR);
        initUser("test", "test", RoleType.USER);
    }

    private void initUser(
            @NotNull final String username,
            @NotNull final String password,
            @NotNull final RoleType roleType
    ) {
        if(userRepository.findOneByLogin(username) == null)
            createUser(username, password, roleType);
    }

    @Transactional
    private void createUser(String username, String password, RoleType roleType) {
        if (username == null || username.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(username);
        user.setPasswordHash(passwordEncoder.encode(password));
        @NotNull final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        @NotNull final List<Role> roles = new ArrayList<>();
        roles.add(role);
        user.setRoles(roles);
        userRepository.save(user);
    }

}
