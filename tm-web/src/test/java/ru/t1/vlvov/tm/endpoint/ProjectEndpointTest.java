package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.client.ProjectEndpointClient;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.model.Project;
import java.util.Collection;

public class ProjectEndpointTest {

    final static String BASE_URL = "http://localhost:8080/api/projects";

    @NotNull
    final ProjectEndpointClient client = ProjectEndpointClient.client(BASE_URL);

    @NotNull
    final Project project1 = new Project("Project1");

    @NotNull
    final Project project2 = new Project("Project2");

    @NotNull
    final Project project3 = new Project("Project3");

    @Before
    public void init() {
        client.save(project1);
        client.save(project2);
        client.save(project3);
    }

    @After
    public void destroy() {
        if (client.findById(project1.getId()) != null)
            client.delete(project1);
        if (client.findById(project2.getId()) != null)
            client.delete(project2);
        if (client.findById(project3.getId()) != null)
            client.delete(project3);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        Project project = new Project("New Project");
        client.save(project);
        Assert.assertEquals(project.getId(), client.findById(project.getId()).getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void delete() {
        Assert.assertNotNull(client.findById(project1.getId()));
        client.delete(project1);
        Assert.assertNull(client.findById(project1.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteById() {
        Assert.assertNotNull(client.findById(project1.getId()));
        client.delete(project1.getId());
        Assert.assertNull(client.findById(project1.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        Collection<Project> projects = client.findAll();
        Assert.assertTrue(projects.size() >= 3);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        Assert.assertNotNull(client.findById(project1.getId()));
    }

}
