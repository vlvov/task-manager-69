package ru.t1.vlvov.tm.exception.field;

public final class LoginEmptyException extends AbstractFieldNotFoundException {

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

}