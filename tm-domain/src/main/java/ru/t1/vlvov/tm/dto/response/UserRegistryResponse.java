package ru.t1.vlvov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
