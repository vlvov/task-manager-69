package ru.t1.vlvov.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldNotFoundException {

    public TaskIdEmptyException() {
        super("Error! Task Id is empty...");
    }

}