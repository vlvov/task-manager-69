package ru.t1.vlvov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
