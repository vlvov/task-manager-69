package ru.t1.vlvov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectRemoveByIdResponse extends AbstractProjectResponse {

    public ProjectRemoveByIdResponse(@Nullable ProjectDTO project) {
        super(project);
    }

}
