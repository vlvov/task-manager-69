package ru.t1.vlvov.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.exception.AbstractException;

public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException() {
        super();
    }

    public AbstractSystemException(@Nullable final String message) {
        super(message);
    }

    public AbstractSystemException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(@Nullable final Throwable cause) {
        super(cause);
    }

    protected AbstractSystemException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            @Nullable final boolean enableSuppression,
            @Nullable final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
