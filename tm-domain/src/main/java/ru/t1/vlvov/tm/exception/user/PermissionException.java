package ru.t1.vlvov.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! You don't have enough rights for this action...");
    }

}