package ru.t1.vlvov.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.t1.vlvov.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(TaskDTO task) {
        super(task);
    }

}
