package ru.t1.vlvov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @NotNull
    @Column(nullable = false, name = "user_id")
    private String userId;

}
