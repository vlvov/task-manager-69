package ru.t1.vlvov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserLogoutRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLogoutRequest(@Nullable String token) {
        super(token);
    }

}
