package ru.t1.vlvov.tm.exception.field;

public final class PasswordEmptyException extends AbstractFieldNotFoundException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}