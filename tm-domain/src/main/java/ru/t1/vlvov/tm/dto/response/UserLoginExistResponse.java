package ru.t1.vlvov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class UserLoginExistResponse extends AbstractResponse {

    boolean isLoginExist = false;

}
