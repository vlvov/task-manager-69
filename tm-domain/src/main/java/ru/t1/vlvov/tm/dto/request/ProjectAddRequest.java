package ru.t1.vlvov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class ProjectAddRequest extends AbstractUserRequest {

    private ProjectDTO project;

    public ProjectAddRequest(@Nullable String token) {
        super(token);
    }

}
