package ru.t1.vlvov.tm.exception.field;

public final class NameEmptyException extends AbstractFieldNotFoundException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}