package ru.t1.vlvov.tm.exception.entity;

import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException() {
        super();
    }

    public AbstractEntityNotFoundException(@Nullable final String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(@Nullable final Throwable cause) {
        super(cause);
    }

    protected AbstractEntityNotFoundException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            @Nullable final boolean enableSuppression,
            @Nullable final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
