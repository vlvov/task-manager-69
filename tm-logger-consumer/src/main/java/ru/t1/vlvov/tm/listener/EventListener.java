package ru.t1.vlvov.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.service.LoggerService;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class EventListener implements MessageListener {

    @NotNull private final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    @NotNull
    @Autowired
    private LoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final String yaml = ((TextMessage) message).getText();
        @NotNull final Map<String, Object> event = mapper.readValue(yaml, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        loggerService.log(table, yaml);
    }

}
