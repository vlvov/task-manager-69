package ru.t1.vlvov.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['buildNumber']}")
    private String applicationVersion;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.user']}")
    private String databaseUser;

    @NotNull
    @Value("#{environment['activemq.host']}")
    private String activeMQHost;

    @NotNull
    @Value("#{environment['database.password']}")
    private String databasePassword;

    @NotNull
    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddl;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @NotNull
    @Value("#{environment['server.port']}")
    private String serverPort;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['developer']}")
    private String authorName;

    @NotNull
    @Value("#{environment['email']}")
    private String authorEmail;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String databaseFormatSql;

    @NotNull
    @Value("#{environment['database.second_lvl_cash']}")
    private String databaseSecondLvlCash;

    @NotNull
    @Value("#{environment['database.factory_class']}")
    private String databaseFactoryClass;

    @NotNull
    @Value("#{environment['database.use_query_cash']}")
    private String databaseUseQueryCash;

    @NotNull
    @Value("#{environment['database.use_min_puts']}")
    private String databaseUseMinPuts;

    @NotNull
    @Value("#{environment['database.region_prefix']}")
    private String databaseRegionPrefix;

    @NotNull
    @Value("#{environment['database.config_file_path']}")
    private String databaseConfigFilePath;

}
