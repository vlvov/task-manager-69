package ru.t1.vlvov.tm.log;

public enum OperationType {

    INSERT,
    DELETE,
    UPDATE;

}
