package ru.t1.vlvov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.dto.model.AbstractModelDTO;

@Repository
public interface AbstractDtoRepository<E extends AbstractModelDTO> extends JpaRepository<E, String> {

}
