package ru.t1.vlvov.tm.api.service;

public interface IDomainService {

    public void loadDataBackup();

    public void saveDataBackup();

    public void loadDataBase64();

    public void saveDataBase64();

    public void loadDataBinary();

    public void saveDataBinary();

    public void loadDataJsonFasterXml();

    public void loadDataJsonJaxB();

    public void saveDataJsonJaxB();

    public void loadDataXmlFasterXml();

    public void loadDataXmlJaxB();

    public void saveDataXmlFasterXml();

    public void saveDataXmlJaxB();

    public void loadDataYamlFasterXml();

    public void saveDataYamlFasterXml();

    public void saveDataJsonFasterXml();

}
