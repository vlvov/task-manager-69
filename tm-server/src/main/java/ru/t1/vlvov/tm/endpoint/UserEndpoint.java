package ru.t1.vlvov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.vlvov.tm.api.endpoint.IUserEndpoint;
import ru.t1.vlvov.tm.api.service.IAuthService;
import ru.t1.vlvov.tm.api.service.dto.IUserDtoService;
import ru.t1.vlvov.tm.api.service.model.IUserService;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.dto.request.*;
import ru.t1.vlvov.tm.dto.response.*;
import ru.t1.vlvov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.vlvov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IUserDtoService userServiceDTO;

    @NotNull
    @Autowired
    private IAuthService authService;

    @Override
    @NotNull
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @NotNull final UserDTO user = userServiceDTO.setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String login = request.getLogin();
        @NotNull final UserDTO user = userServiceDTO.lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    ) {
        check(request);
        @Nullable final String login = request.getLogin();
        @NotNull final UserDTO user = userServiceDTO.unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        check(request);
        @Nullable final String login = request.getLogin();
        @Nullable final UserDTO user = userServiceDTO.findByLogin(login);
        userService.removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @NotNull final UserDTO user = userServiceDTO.updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final UserDTO user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final UserDTO user = userServiceDTO.findOneById(userId);
        return new UserProfileResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLoginExistResponse isUserLoginExist(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginExistRequest request
    ) {
        @NotNull final UserLoginExistResponse userLoginExistResponse = new UserLoginExistResponse();
        userLoginExistResponse.setLoginExist(userServiceDTO.isLoginExist(request.getLogin()));
        return userLoginExistResponse;
    }

}
