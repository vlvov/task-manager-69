package ru.t1.vlvov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.vlvov.tm.dto.model.AbstractUserOwnedModelDTO;

public interface AbstractUserOwnedDtoRepository<E extends AbstractUserOwnedModelDTO> extends JpaRepository<E, String> {

}
