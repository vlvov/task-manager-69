package ru.t1.vlvov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @Nullable
    List<Task> findAllByProjectId(@NotNull final String projectId);

    @Nullable
    List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Nullable Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @Nullable Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name);

    @Nullable
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @NotNull String description
    );

}
