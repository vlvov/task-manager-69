package ru.t1.vlvov.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.log.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class JMSLoggerProducer {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    private static final String QUEUE = "LOGGER";

    private final ConnectionFactory connectionFactory;

    private final Connection connection;

    private final Session session;

    private final Queue destination;

    private final MessageProducer messageProducer;

    private final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @SneakyThrows
    public JMSLoggerProducer() {
        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
        messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
    }

    public void send(@NotNull final OperationEvent event) {
        executorService.submit(() -> sync(event));
    }

    @SneakyThrows
    private void sync(@NotNull final OperationEvent event) {
        final Class entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
            @NotNull final Table table = (Table) annotation;
            event.setTable(table.name());
        }
        @NotNull final String yaml = mapper.writeValueAsString(event);
        send(yaml);
    }

}
