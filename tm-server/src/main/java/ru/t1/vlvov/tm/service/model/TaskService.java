package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.service.model.ITaskService;
import ru.t1.vlvov.tm.enumerated.CustomSort;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.exception.field.NameEmptyException;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.repository.model.TaskRepository;

import java.util.Collection;
import java.util.List;

@Service
public final class TaskService extends AbstractUserOwnedService<Task> implements ITaskService {

    @Autowired
    @NotNull
    private TaskRepository taskRepository;

    @Override
    @NotNull
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task model = new Task();
        model.setName(name);
        taskRepository.save(model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task model = new Task();
        model.setName(name);
        model.setDescription(description);
        taskRepository.save(model);
        return model;
    }

    @Override
    @Nullable
    public List<Task> findAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    @Nullable
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    @NotNull
    @Transactional
    public Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final Task model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        taskRepository.save(model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final Task model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        taskRepository.save(model);
        return model;
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void add(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<Task> collection) {
        clear();
        taskRepository.saveAll(collection);
    }

    @Override
    @Nullable
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.existsById(id);
    }

    @Override
    public @Nullable Task findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findFirstById(id);
    }

    @Override
    @Transactional
    public void add(@Nullable String userId, @Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        taskRepository.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable String userId, @Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        taskRepository.save(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public @Nullable Task findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return taskRepository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable String userId, @Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        taskRepository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        Task model = findOneById(userId, id);
        remove(userId, model);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable String userId, @Nullable CustomSort sort) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (sort == null) return findAll(userId);
        final Sort sortable = Sort.by(Sort.Direction.ASC, CustomSort.getOrderByField(sort));
        return taskRepository.findAllByUserId(userId, sortable);
    }

}
