package ru.t1.vlvov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDTO> {

    @Nullable
    UserDTO create(@Nullable final String login, @Nullable final String password);

    @Nullable
    UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email);

    @Nullable
    UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role);

    @Nullable
    UserDTO findByLogin(@Nullable final String login);

    @Nullable
    UserDTO findByEmail(@Nullable final String email);

    @NotNull
    UserDTO removeByLogin(@Nullable final String login);

    @NotNull
    UserDTO setPassword(@Nullable final String id, @Nullable final String password);

    @NotNull
    UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    );

    boolean isLoginExist(@Nullable final String login);

    boolean isEmailExist(@Nullable final String email);

    @NotNull
    UserDTO lockUserByLogin(@Nullable final String id);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable final String id);

}
