package ru.t1.vlvov.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.CustomSort;
import ru.t1.vlvov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void add(@Nullable String userId, @Nullable M model);

    void update(@Nullable String userId, @Nullable M model);

    void clear(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable M findOneById(@Nullable String userId, @Nullable String id);

    void remove(@Nullable String userId, @Nullable M model);

    void removeById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable CustomSort sort);

}
