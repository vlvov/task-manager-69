package ru.t1.vlvov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.model.Project;

import java.util.List;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    @Nullable
    Project findFirstById(@NotNull final String id);

    void deleteAllByUserId(@NotNull final String userId);

    @Nullable
    List<Project> findAllByUserId(@NotNull final String userId);

    @Nullable
    Project findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<Project> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

}
