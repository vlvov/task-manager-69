package ru.t1.vlvov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    Session findFirstById(@NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

    void deleteAllByUserId(@NotNull final String userId);

    @Nullable
    List<Session> findAllByUserId(@NotNull final String userId);

    @Nullable
    Session findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<Session> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

}
