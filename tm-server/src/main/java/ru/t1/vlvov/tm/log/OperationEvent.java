package ru.t1.vlvov.tm.log;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class OperationEvent {

    @NotNull
    private final Object entity;

    @NotNull
    private final Long timestamp = System.currentTimeMillis();

    @NotNull
    private String table;

    @NotNull
    private final OperationType operationType;

    public OperationEvent(@NotNull final Object entity, @NotNull final OperationType operationType) {
        this.entity = entity;
        this.operationType = operationType;
    }

}
