package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import ru.t1.vlvov.tm.dto.model.SessionDTO;

import java.util.List;

public interface SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> {

    SessionDTO findFirstById(@NotNull final String id);

    @Nullable
    List<SessionDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    SessionDTO findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<SessionDTO> findAll();

    void deleteAllByUserId(@NotNull final String userId);

    @Nullable
    List<SessionDTO> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

}
