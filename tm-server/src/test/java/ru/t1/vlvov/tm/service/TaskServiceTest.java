package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.dto.IProjectDtoService;
import ru.t1.vlvov.tm.api.service.dto.ITaskDtoService;
import ru.t1.vlvov.tm.api.service.dto.IUserDtoService;
import ru.t1.vlvov.tm.api.service.model.IUserService;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.service.dto.ProjectDtoService;
import ru.t1.vlvov.tm.service.dto.TaskDtoService;
import ru.t1.vlvov.tm.service.dto.UserDtoService;
import ru.t1.vlvov.tm.service.model.UserService;

import static ru.t1.vlvov.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.vlvov.tm.constant.ProjectTestData.USER2_PROJECT1;
import static ru.t1.vlvov.tm.constant.TaskTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskDtoService taskService = new TaskDtoService(connectionService);

    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @NotNull
    private final IUserDtoService userServiceDTO = new UserDtoService(connectionService, propertyService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Before
    public void init() {
        userServiceDTO.add(USER1);
        userServiceDTO.add(USER2);
        userServiceDTO.add(ADMIN1);
        projectService.add(USER1_PROJECT1);
        projectService.add(USER2_PROJECT1);
    }

    @After
    public void tearDown() {
        userService.removeById(USER1.getId());
        userService.removeById(USER2.getId());
        userService.removeById(ADMIN1.getId());
    }

    @Test
    public void add() {
        taskService.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findOneById(USER1_TASK1.getId()).getId());
    }

    @Test
    public void addByUserId() {
        taskService.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findOneById(USER1_TASK1.getId()).getId());
    }

    @Test
    public void clearByUserId() {
        for (final TaskDTO task : USER1_TASK_LIST) taskService.add(task);
        Assert.assertFalse(taskService.findAll(USER1.getId()).isEmpty());
        taskService.clear(USER2.getId());
        Assert.assertFalse(taskService.findAll(USER1.getId()).isEmpty());
        taskService.clear(USER1.getId());
        Assert.assertTrue(taskService.findAll(USER1.getId()).isEmpty());
        taskService.add(USER2_TASK1);
        taskService.clear(USER1.getId());
        Assert.assertFalse(taskService.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        for (final TaskDTO task : TASK_LIST) taskService.add(task);
        Assert.assertFalse(taskService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void findOneByIdByUserId() {
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findOneById(USER1.getId(), USER1_TASK1.getId()).getId());
        Assert.assertNull(taskService.findOneById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void removeByUserId() {
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        taskService.remove(USER1.getId(), USER1_TASK1);
        Assert.assertNull(taskService.findOneById(USER1_TASK1.getId()));
        Assert.assertFalse(taskService.findAll().contains(USER1_TASK1));
        Assert.assertNotNull(taskService.findOneById(USER2_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        taskService.removeById(USER1.getId(), USER1_TASK1.getId());
        Assert.assertNull(taskService.findOneById(USER1_TASK1.getId()));
        Assert.assertEquals(USER2_TASK1.getId(), taskService.findOneById(USER2_TASK1.getId()).getId());
    }

    @Test
    public void existsByIdByUserId() {
        taskService.add(USER1_TASK1);
        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2_TASK1.getId()));
    }

    @Test
    public void changeTaskStatusById() {
        taskService.add(USER1_TASK1);
        Assert.assertEquals(Status.NOT_STARTED, USER1_TASK1.getStatus());
        taskService.changeTaskStatusById(USER1_TASK1.getUserId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findOneById(USER1_TASK1.getId()).getStatus());
    }

    @Test
    public void createTaskName() {
        @NotNull TaskDTO task = taskService.create(USER1.getId(), "test_task");
        Assert.assertEquals(task.getId(), taskService.findOneById(task.getId()).getId());
        Assert.assertEquals("test_task", task.getName());
        Assert.assertEquals(USER1.getId(), task.getUserId());
    }

    @Test
    public void createTaskNameDescription() {
        @NotNull TaskDTO task = taskService.create(USER1.getId(), "test_task", "test_description");
        Assert.assertEquals(task.getId(), taskService.findOneById(task.getId()).getId());
        Assert.assertEquals("test_task", task.getName());
        Assert.assertEquals("test_description", task.getDescription());
        Assert.assertEquals(USER1.getId(), task.getUserId());
    }

    @Test
    public void updateById() {
        @NotNull TaskDTO task = taskService.create(USER1.getId(), "test_task", "test_description");
        taskService.updateById(USER1.getId(), task.getId(), "new name", "new description");
        Assert.assertEquals("new name", taskService.findOneById(task.getId()).getName());
        Assert.assertEquals("new description", taskService.findOneById(task.getId()).getDescription());
    }

}
