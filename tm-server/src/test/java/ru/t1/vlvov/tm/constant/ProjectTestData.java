package ru.t1.vlvov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ProjectTestData {

    @NotNull
    public final static ProjectDTO USER1_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER1_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER1_PROJECT3 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER2_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER2_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER2_PROJECT3 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN1_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN1_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO ADMIN1_PROJECT3 = new ProjectDTO();

    @NotNull
    public final static List<ProjectDTO> USER1_PROJECT_LIST = Arrays.asList(USER1_PROJECT1, USER1_PROJECT2, USER1_PROJECT3);

    @NotNull
    public final static List<ProjectDTO> USER2_PROJECT_LIST = Arrays.asList(USER2_PROJECT1, USER2_PROJECT2, USER2_PROJECT3);

    @NotNull
    public final static List<ProjectDTO> ADMIN1_PROJECT_LIST = Arrays.asList(ADMIN1_PROJECT1, ADMIN1_PROJECT2, ADMIN1_PROJECT3);

    @NotNull
    public final static List<ProjectDTO> PROJECT_LIST = new ArrayList<>();

    static {
        USER1_PROJECT_LIST.forEach(project -> project.setUserId(UserTestData.USER1.getId()));
        USER2_PROJECT_LIST.forEach(project -> project.setUserId(UserTestData.USER2.getId()));
        ADMIN1_PROJECT_LIST.forEach(project -> project.setUserId(UserTestData.ADMIN1.getId()));

        PROJECT_LIST.addAll(USER1_PROJECT_LIST);
        PROJECT_LIST.addAll(USER2_PROJECT_LIST);
        PROJECT_LIST.addAll(ADMIN1_PROJECT_LIST);

        PROJECT_LIST.forEach(project -> project.setId("t-0" + PROJECT_LIST.indexOf(project)));
        PROJECT_LIST.forEach(project -> project.setName("project-" + PROJECT_LIST.indexOf(project)));
        PROJECT_LIST.forEach(project -> project.setDescription("description of project-" + PROJECT_LIST.indexOf(project)));
    }

}
