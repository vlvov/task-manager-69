package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.vlvov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.vlvov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.vlvov.tm.api.repository.model.IUserRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.vlvov.tm.repository.dto.TaskDtoRepository;
import ru.t1.vlvov.tm.repository.dto.UserDtoRepository;
import ru.t1.vlvov.tm.repository.model.UserRepository;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.vlvov.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.vlvov.tm.constant.ProjectTestData.USER2_PROJECT1;
import static ru.t1.vlvov.tm.constant.TaskTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private ITaskDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN1);
            entityManager.getTransaction().commit();

            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            entityManager.getTransaction().commit();

        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeById(USER1.getId());
            repository.removeById(USER2.getId());
            repository.removeById(ADMIN1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
            taskRepository.add(USER1_TASK1);
            Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1_TASK1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.add(USER1_TASK1);
            Assert.assertEquals(USER1.getId(), taskRepository.findOneById(USER1_TASK1.getId()).getUserId());
            Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1_TASK1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.add(USER1_TASK1);
            taskRepository.add(USER2_TASK1);
            taskRepository.clear(USER1.getId());
            Assert.assertTrue(taskRepository.findAll(USER1.getId()).isEmpty());
            Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findOneById(USER2_TASK1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (final TaskDTO task : TASK_LIST) {
                taskRepository.add(task);
            }
            Assert.assertFalse(taskRepository.findAll(USER1.getId()).isEmpty());
            taskRepository.clear(USER1.getId());
            Assert.assertTrue(taskRepository.findAll(USER1.getId()).isEmpty());
            Assert.assertFalse(taskRepository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.add(USER1_TASK1);
            taskRepository.add(USER2_TASK1);
            Assert.assertEquals(USER1_TASK1.getId(), taskRepository.findOneById(USER1.getId(), USER1_TASK1.getId()).getId());
            Assert.assertNull(taskRepository.findOneById(USER1.getId(), USER2_TASK1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.add(USER1_TASK1);
            taskRepository.add(USER2_TASK1);
            taskRepository.removeById(USER1.getId(), USER1_TASK1.getId());
            Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
            Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findOneById(USER2_TASK1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.add(USER1_TASK1);
            taskRepository.add(USER2_TASK1);
            taskRepository.removeById(USER1.getId(), USER1_TASK1.getId());
            Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
            Assert.assertEquals(USER2_TASK1.getId(), taskRepository.findOneById(USER2_TASK1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.add(USER1_TASK1);
            Assert.assertNotNull(taskRepository.findOneById(USER1_TASK1.getId()));
            Assert.assertNull(taskRepository.findOneById(USER2_TASK1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByProjectIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (final TaskDTO task : USER1_TASK_LIST) {
                taskRepository.add(task);
            }
            for (final TaskDTO task : USER2_TASK_LIST) {
                taskRepository.add(task);
            }
            Assert.assertFalse(taskRepository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()).isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
