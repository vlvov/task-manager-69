package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.repository.dto.UserDtoRepository;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;
import ru.t1.vlvov.tm.util.HashUtil;

import javax.persistence.EntityManager;

import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private IUserDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDtoRepository(entityManager);
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.clear();
            entityManager.getTransaction().commit();
            Assert.assertTrue(repository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (final UserDTO user : USER_LIST) {
                repository.add(user);
            }
            entityManager.getTransaction().commit();
            Assert.assertFalse(repository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
            repository.remove(USER1);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            Assert.assertEquals(USER1.getId(), repository.findOneById(USER1.getId()).getId());
            repository.removeById(USER1.getId());
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            Assert.assertNotNull(repository.findOneById(USER1.getId()));
            repository.removeById(USER1.getId());
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void createLoginPassword() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNull(repository.findByLogin("USER1_LOGIN"));
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            repository.add(user);
            entityManager.getTransaction().commit();
            Assert.assertNotNull(repository.findByLogin("USER1_LOGIN"));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void createLoginPasswordEmail() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNull(repository.findByLogin("USER1_LOGIN"));
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            user.setEmail("USER1_EMAIL");
            repository.add(user);
            entityManager.getTransaction().commit();
            Assert.assertNotNull(repository.findByLogin("USER1_LOGIN"));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void createLoginPasswordRole() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNull(repository.findByLogin("USER1_LOGIN"));
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            user.setRole(Role.ADMIN);
            repository.add(user);
            entityManager.getTransaction().commit();
            Assert.assertNotNull(repository.findByLogin("USER1_LOGIN"));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            repository.add(user);
            entityManager.getTransaction().commit();
            Assert.assertEquals(user.getId(), repository.findByLogin(user.getLogin()).getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("USER1_LOGIN");
            user.setPasswordHash(HashUtil.salt("USER1_PASSWORD", propertyService));
            user.setEmail("USER1_EMAIL");
            repository.add(user);
            entityManager.getTransaction().commit();
            Assert.assertEquals(user.getId(), repository.findByEmail(user.getEmail()).getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
