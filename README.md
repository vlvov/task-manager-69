# TASK-MANAGER

## DEVELOPER

**NAME**: Vladimir Lvov

**E-MAIL**: vlvov@t1-consilting.com

**E-MAIL**: vlvov@t2-consilting.com

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: Windows 10

## HARDWARE

**CPU**: i5

**RAM**: 8Gb

**HDD**: 1000Gb

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```

## APPLICATION BUILD

```
mvn clean install
```
