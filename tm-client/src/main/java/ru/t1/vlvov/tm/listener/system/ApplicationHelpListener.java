package ru.t1.vlvov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.Collection;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-h";

    @NotNull
    private final String DESCRIPTION = "Show application commands.";

    @NotNull
    private final String NAME = "help";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractListener> commands = getListeners();
        for (@NotNull final AbstractListener command : commands) System.out.println(command);
    }

}
