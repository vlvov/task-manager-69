package ru.t1.vlvov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @NotNull
    private final String DESCRIPTION = "Show task by Id.";

    @NotNull
    private final String NAME = "task-show-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable final TaskDTO task = taskEndpoint.getTaskById(request).getTask();
        showTask(task);
    }

}