package ru.t1.vlvov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectListener extends AbstractTaskListener {

    @NotNull
    private final String DESCRIPTION = "Unbind task from Project.";

    @NotNull
    private final String NAME = "task-unbind-from-project";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskUnbindFromProjectListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setTaskId(taskId);
        request.setProjectId(projectId);
        taskEndpoint.unbindTaskFromProject(request);
    }

}