package ru.t1.vlvov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataYamlLoadFasterXmlRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class DataYamlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Load data from yaml file.";

    @NotNull
    private final String NAME = "data-load-yaml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataYamlLoadFasterXmlListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA LOAD YAML]");
        @NotNull DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(getToken());
        domainEndpoint.loadDataYamlFasterXml(request);
    }

}
