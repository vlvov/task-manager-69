package ru.t1.vlvov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.UserLockRequest;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.util.TerminalUtil;

@Component
public final class UserLockListener extends AbstractUserListener {

    @NotNull
    private final String NAME = "user-lock";

    @NotNull
    private final String DESCRIPTION = "Lock user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@userLockListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);
        userEndpoint.lockUser(request);
    }

}
