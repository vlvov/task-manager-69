package ru.t1.vlvov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.TaskRemoveByIdRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdListener extends AbstractTaskListener {

    @NotNull
    private final String DESCRIPTION = "Remove task by Id.";

    @NotNull
    private final String NAME = "task-remove-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setTaskId(id);
        taskEndpoint.removeTaskById(request);
    }

}