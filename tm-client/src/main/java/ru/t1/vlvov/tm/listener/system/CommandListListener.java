package ru.t1.vlvov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.listener.IListener;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-cmd";

    @NotNull
    private final String DESCRIPTION = "Show commands.";

    @NotNull
    private final String NAME = "commands";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@commandListListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @NotNull final Collection<AbstractListener> commands = getListeners();
        System.out.println("[COMMANDS]");
        for (@NotNull final IListener command : commands) {
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
