package ru.t1.vlvov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.ServerVersionRequest;
import ru.t1.vlvov.tm.dto.response.ServerVersionResponse;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-v";

    @NotNull
    private final String DESCRIPTION = "Show application version.";

    @NotNull
    private final String NAME = "version";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[VERSION]");
        @Nullable final ServerVersionRequest request = new ServerVersionRequest();
        @NotNull ServerVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

}
