package ru.t1.vlvov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataXmlSaveFasterXmlRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class DataXmlSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Save data to xml file.";

    @NotNull
    private final String NAME = "data-save-xml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataXmlSaveFasterXmlListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA SAVE XML]");
        @NotNull DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(getToken());
        domainEndpoint.saveDataXmlFasterXml(request);
    }

}