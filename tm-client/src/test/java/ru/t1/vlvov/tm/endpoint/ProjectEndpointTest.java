package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.vlvov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.request.*;
import ru.t1.vlvov.tm.dto.response.ProjectCreateResponse;
import ru.t1.vlvov.tm.dto.response.ProjectGetByIdResponse;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    ProjectCreateResponse responseCreateProject;

    @Nullable
    private String userToken;

    @Before
    public void setUp() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin("test");
        request.setPassword("test");
        userToken = authEndpoint.login(request).getToken();

        @Nullable final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(userToken);
        requestCreateProject.setName("name");
        requestCreateProject.setDescription("description");
        responseCreateProject = projectEndpoint.createProject(requestCreateProject);
    }

    @After
    public void tearDown() {
        @Nullable final ProjectClearRequest requestClear = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(requestClear);
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
    }

    @Test
    public void projectCreate() {
        @Nullable final ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        request.setName("name");
        request.setDescription("description");
        @Nullable final ProjectCreateResponse response = projectEndpoint.createProject(request);
        ProjectDTO project = response.getProject();
        Assert.assertEquals("name", project.getName());
        Assert.assertEquals("description", project.getDescription());
    }

    @Test
    public void projectChangeStatusById() {
        String projectId = responseCreateProject.getProject().getId();
        @Nullable final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setProjectId(projectId);
        request.setStatus("In progress");
        ProjectDTO project = projectEndpoint.changeProjectStatusById(request).getProject();
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void projectClear() {
        Assert.assertFalse(projectEndpoint.listProject(new ProjectListRequest(userToken)).getProjects().isEmpty());
        @Nullable final ProjectClearRequest request = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(request);
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(userToken)).getProjects());
    }

    @Test
    public void projectList() {
        @Nullable final ProjectCreateRequest requestProjectCreate = new ProjectCreateRequest(userToken);
        requestProjectCreate.setName("name2");
        requestProjectCreate.setDescription("description2");
        projectEndpoint.createProject(requestProjectCreate);
        Assert.assertFalse(projectEndpoint.listProject(new ProjectListRequest(userToken)).getProjects().isEmpty());
    }

    @Test
    public void projectRemoveById() {
        Assert.assertFalse(projectEndpoint.listProject(new ProjectListRequest(userToken)).getProjects().isEmpty());
        @Nullable final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setProjectId(responseCreateProject.getProject().getId());
        projectEndpoint.removeById(request);
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(userToken)).getProjects());
    }

    @Test
    public void projectShowById() {
        @Nullable final ProjectGetByIdRequest requestGetById = new ProjectGetByIdRequest(userToken);
        requestGetById.setProjectId(responseCreateProject.getProject().getId());
        @Nullable final ProjectGetByIdResponse responseGetById = projectEndpoint.getById(requestGetById);
        Assert.assertEquals(responseCreateProject.getProject().getId(), responseGetById.getProject().getId());
    }

    @Test
    public void projectUpdateById() {
        String projectId = responseCreateProject.getProject().getId();
        @Nullable final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setProjectId(projectId);
        request.setDescription("new description");
        request.setName("new name");
        ProjectDTO project = projectEndpoint.updateById(request).getProject();
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new description", project.getDescription());
    }

}
