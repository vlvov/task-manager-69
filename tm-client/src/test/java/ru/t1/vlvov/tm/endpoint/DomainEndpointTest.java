package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.vlvov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.vlvov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.request.*;
import ru.t1.vlvov.tm.dto.response.ProjectCreateResponse;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @NotNull
    ProjectDTO project;

    @Before
    public void setUp() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin("admin");
        request.setPassword("admin");
        adminToken = authEndpoint.login(request).getToken();
        request.setLogin("test");
        request.setPassword("test");
        userToken = authEndpoint.login(request).getToken();
        project = addProject("name", "description");
    }

    @After
    public void tearDown() {
        @Nullable final ProjectClearRequest requestClearProject = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(requestClearProject);
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
        @NotNull final UserLogoutRequest requestAdminLogout = new UserLogoutRequest(adminToken);
        authEndpoint.logout(requestAdminLogout);
    }

    public ProjectDTO addProject(final String name, final String description) {
        @Nullable final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(userToken);
        requestCreateProject.setName(name);
        requestCreateProject.setDescription(description);
        @NotNull final ProjectCreateResponse responseCreateProject = projectEndpoint.createProject(requestCreateProject);
        return responseCreateProject.getProject();
    }

    @Test
    public void saveDataBackup() {
        @NotNull DataBackupSaveRequest backupSaveRequest = new DataBackupSaveRequest(adminToken);
        domainEndpoint.saveDataBackup(backupSaveRequest);
        @Nullable final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(userToken);
        removeByIdRequest.setProjectId(project.getId());
        projectEndpoint.removeById(removeByIdRequest);
        @Nullable final ProjectGetByIdRequest requestGetById = new ProjectGetByIdRequest(userToken);
        requestGetById.setProjectId(project.getId());
        Assert.assertNull(projectEndpoint.getById(requestGetById).getProject());
        @NotNull DataBackupLoadRequest backupLoadRequest = new DataBackupLoadRequest(adminToken);
        domainEndpoint.loadDataBackup(backupLoadRequest);
        requestGetById.setProjectId(project.getId());
        Assert.assertEquals(project.getId(), projectEndpoint.getById(requestGetById).getProject().getId());
    }

    @Test
    public void dataBase64SaveLoad() {
        @NotNull DataBase64SaveRequest base64SaveRequest = new DataBase64SaveRequest(adminToken);
        domainEndpoint.saveDataBase64(base64SaveRequest);
        @Nullable final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(userToken);
        removeByIdRequest.setProjectId(project.getId());
        projectEndpoint.removeById(removeByIdRequest);
        @Nullable final ProjectGetByIdRequest requestGetById = new ProjectGetByIdRequest(userToken);
        requestGetById.setProjectId(project.getId());
        Assert.assertNull(projectEndpoint.getById(requestGetById).getProject());
        @NotNull DataBase64LoadRequest base64LoadRequest = new DataBase64LoadRequest(adminToken);
        domainEndpoint.loadDataBase64(base64LoadRequest);
        requestGetById.setProjectId(project.getId());
        Assert.assertEquals(project.getId(), projectEndpoint.getById(requestGetById).getProject().getId());
    }

    @Test
    public void dataJsonLoadFasterXmlSaveLoad() {
        @NotNull DataJsonSaveFasterXmlRequest jsonSaveFasterXmlRequest = new DataJsonSaveFasterXmlRequest(adminToken);
        domainEndpoint.saveDataJsonFasterXml(jsonSaveFasterXmlRequest);

        @Nullable final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(userToken);
        removeByIdRequest.setProjectId(project.getId());
        projectEndpoint.removeById(removeByIdRequest);
        @Nullable final ProjectGetByIdRequest requestGetById = new ProjectGetByIdRequest(userToken);
        requestGetById.setProjectId(project.getId());
        Assert.assertNull(projectEndpoint.getById(requestGetById).getProject());

        @NotNull DataJsonLoadFasterXmlRequest jsonLoadFasterXmlRequest = new DataJsonLoadFasterXmlRequest(adminToken);
        domainEndpoint.loadDataJsonFasterXml(jsonLoadFasterXmlRequest);

        requestGetById.setProjectId(project.getId());
        Assert.assertEquals(project.getId(), projectEndpoint.getById(requestGetById).getProject().getId());
    }

    @Test
    public void dataXmlSaveJaxBSaveLoad() {
        @NotNull DataXmlSaveJaxBRequest xmlSaveJaxBRequest = new DataXmlSaveJaxBRequest(adminToken);
        domainEndpoint.saveDataXmlJaxB(xmlSaveJaxBRequest);

        @Nullable final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(userToken);
        removeByIdRequest.setProjectId(project.getId());
        projectEndpoint.removeById(removeByIdRequest);
        @Nullable final ProjectGetByIdRequest requestGetById = new ProjectGetByIdRequest(userToken);
        requestGetById.setProjectId(project.getId());
        Assert.assertNull(projectEndpoint.getById(requestGetById).getProject());

        @NotNull DataXmlLoadJaxBRequest xmlLoadJaxBRequest = new DataXmlLoadJaxBRequest(adminToken);
        domainEndpoint.loadDataXmlJaxB(xmlLoadJaxBRequest);

        requestGetById.setProjectId(project.getId());
        Assert.assertEquals(project.getId(), projectEndpoint.getById(requestGetById).getProject().getId());
    }

    @Test
    public void dataYamlSaveFasterXmlSaveLoad() {
        @NotNull DataYamlSaveFasterXmlRequest yamlSaveFasterXmlRequest = new DataYamlSaveFasterXmlRequest(adminToken);
        domainEndpoint.saveDataYamlFasterXml(yamlSaveFasterXmlRequest);

        @Nullable final ProjectRemoveByIdRequest removeByIdRequest = new ProjectRemoveByIdRequest(userToken);
        removeByIdRequest.setProjectId(project.getId());
        projectEndpoint.removeById(removeByIdRequest);
        @Nullable final ProjectGetByIdRequest requestGetById = new ProjectGetByIdRequest(userToken);
        requestGetById.setProjectId(project.getId());
        Assert.assertNull(projectEndpoint.getById(requestGetById).getProject());

        @NotNull DataYamlLoadFasterXmlRequest yamlLoadFasterXmlRequest = new DataYamlLoadFasterXmlRequest(adminToken);
        domainEndpoint.loadDataYamlFasterXml(yamlLoadFasterXmlRequest);

        requestGetById.setProjectId(project.getId());
        Assert.assertEquals(project.getId(), projectEndpoint.getById(requestGetById).getProject().getId());
    }

}
